﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace bum
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public struct Tlacitka
        {
            public int x;
            public int y;
            public int hodnota;
            public Button reference;
            public int BylJsemTu;
        }



        private Queue<Tlacitka> Fronta = new Queue<Tlacitka>();
        private Grid GridHra;
        private StackPanel StackP;
        private Pole hra;
        private int Last;
        private int Round = 0;
        private int NewGame = 0;
        private TextBlock bombcounter;
        private Button NEW;
        public int bomb;
        public int bombO;
        private int konec = 0;
        private int odkryto = 0;
        private int PoPrvnimTahu = 0;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void OdhalPole(Tlacitka c)
        {
            Tlacitka UkladaniDoFronty = new Tlacitka();
            Button odkryti = new Button();
            odkryti = c.reference;
            odkryti.Background = Brushes.LightGreen;
            hra.hracipole[c.x, c.y].BylJsemTu = 1;
            if ((c.x - 1 >= 0 && c.y - 1 >= 0) && (c.x - 1 < hra.hracipole.GetLength(0) && c.y - 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x - 1, c.y - 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x - 1, c.y - 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x - 1, c.y - 1].hodnota;
                UkladaniDoFronty.x = c.x - 1;
                UkladaniDoFronty.y = c.y - 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x  >= 0 && c.y - 1 >= 0) && (c.x  < hra.hracipole.GetLength(0) && c.y - 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x , c.y - 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x , c.y - 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x , c.y - 1].hodnota;
                UkladaniDoFronty.x = c.x ;
                UkladaniDoFronty.y = c.y - 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x + 1>= 0 && c.y - 1 >= 0) && (c.x + 1< hra.hracipole.GetLength(0) && c.y - 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x + 1, c.y - 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x + 1, c.y - 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x + 1, c.y - 1].hodnota;
                UkladaniDoFronty.x = c.x + 1;
                UkladaniDoFronty.y = c.y - 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x - 1 >= 0 && c.y  >= 0) && (c.x - 1 < hra.hracipole.GetLength(0) && c.y  < hra.hracipole.GetLength(1)) && hra.hracipole[c.x - 1, c.y ].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x - 1, c.y ].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x - 1, c.y ].hodnota;
                UkladaniDoFronty.x = c.x - 1;
                UkladaniDoFronty.y = c.y;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x + 1 >= 0 && c.y >= 0) && (c.x + 1 < hra.hracipole.GetLength(0) && c.y < hra.hracipole.GetLength(1)) && hra.hracipole[c.x + 1, c.y].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x + 1, c.y].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x + 1, c.y].hodnota;
                UkladaniDoFronty.x = c.x + 1;
                UkladaniDoFronty.y = c.y;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x - 1 >= 0 && c.y + 1 >= 0) && (c.x - 1 < hra.hracipole.GetLength(0) && c.y + 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x - 1, c.y + 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x - 1, c.y + 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x - 1, c.y + 1].hodnota;
                UkladaniDoFronty.x = c.x - 1;
                UkladaniDoFronty.y = c.y + 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x  >= 0 && c.y + 1 >= 0) && (c.x  < hra.hracipole.GetLength(0) && c.y + 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x , c.y + 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x, c.y + 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x, c.y + 1].hodnota;
                UkladaniDoFronty.x = c.x ;
                UkladaniDoFronty.y = c.y + 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            if ((c.x + 1 >= 0 && c.y + 1 >= 0) && (c.x + 1 < hra.hracipole.GetLength(0) && c.y + 1 < hra.hracipole.GetLength(1)) && hra.hracipole[c.x + 1, c.y + 1].hodnota != -1)
            {
                odkryti = hra.hracipole[c.x + 1, c.y + 1].reference;
                odkryti.Background = Brushes.LightGreen;
                //odkryti.Content = hra.hracipole[c.x + 1, c.y + 1].hodnota;
                UkladaniDoFronty.x = c.x + 1;
                UkladaniDoFronty.y = c.y + 1;
                Fronta.Enqueue(UkladaniDoFronty);
                //odkryto++;
            }
            odkryto++;
        }

        private void OdhaleniHran(Tlacitka c)
        {
            Tlacitka Momentalni = new Tlacitka();
            Button Prohledavani_btn = new Button();
            while (Fronta.Count != 0)
            {
                Momentalni = Fronta.Dequeue();
                
                
                if ((Momentalni.x >= 0 && Momentalni.y - 1 >= 0) && (Momentalni.x < hra.hracipole.GetLength(0) && Momentalni.y - 1 < hra.hracipole.GetLength(1)) && hra.hracipole[Momentalni.x, Momentalni.y - 1].hodnota != -1)
                {
                    
                    if (hra.hracipole[Momentalni.x, Momentalni.y - 1].BylJsemTu < 1)
                    { 
                        if (hra.hracipole[Momentalni.x, Momentalni.y - 1].hodnota == 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x, Momentalni.y - 1].reference;
                            
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            Momentalni.reference = Prohledavani_btn;
                            Momentalni.hodnota = hra.hracipole[Momentalni.x, Momentalni.y - 1].hodnota;
                            Momentalni.y = Momentalni.y - 1;
                            Fronta.Enqueue(Momentalni);
                            Momentalni.y = Momentalni.y + 1;
                            odkryto++;
                        }
                        int k = hra.hracipole[Momentalni.x, Momentalni.y - 1].hodnota;
                        
                        if (k > 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x, Momentalni.y - 1].reference;
                            Prohledavani_btn.Content = hra.hracipole[Momentalni.x, Momentalni.y - 1].hodnota;
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            odkryto++;
                        }
                    }
                    hra.hracipole[Momentalni.x, Momentalni.y - 1].BylJsemTu++; 
                }
                
                if ((Momentalni.x + 1>= 0 && Momentalni.y >= 0) && (Momentalni.x + 1< hra.hracipole.GetLength(0) && Momentalni.y  < hra.hracipole.GetLength(1)) && hra.hracipole[Momentalni.x + 1, Momentalni.y].hodnota != -1 )
                {
                    
                    if (hra.hracipole[Momentalni.x + 1, Momentalni.y].BylJsemTu < 1)
                    {
                        if (hra.hracipole[Momentalni.x + 1, Momentalni.y].hodnota == 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x + 1, Momentalni.y].reference;
                            
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            Momentalni.reference = Prohledavani_btn;
                            Momentalni.hodnota = hra.hracipole[Momentalni.x + 1, Momentalni.y].hodnota;
                            Momentalni.x = Momentalni.x + 1;
                            Fronta.Enqueue(Momentalni);
                            Momentalni.x = Momentalni.x - 1;
                            odkryto++;
                        }
                        int k = hra.hracipole[Momentalni.x + 1, Momentalni.y].hodnota;
                        
                        if (k > 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x + 1, Momentalni.y].reference;
                            Prohledavani_btn.Content = hra.hracipole[Momentalni.x + 1, Momentalni.y].hodnota;
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            odkryto++;
                        }
                    }
                    hra.hracipole[Momentalni.x + 1, Momentalni.y].BylJsemTu++;
                }
                
                if ((Momentalni.x - 1 >= 0 && Momentalni.y >= 0) && (Momentalni.x - 1 < hra.hracipole.GetLength(0) && Momentalni.y < hra.hracipole.GetLength(1)) && hra.hracipole[Momentalni.x - 1, Momentalni.y].hodnota != -1 )
                {
                    
                    if (hra.hracipole[Momentalni.x - 1, Momentalni.y].BylJsemTu < 1)
                    { 
                        if (hra.hracipole[Momentalni.x - 1, Momentalni.y].hodnota == 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x - 1, Momentalni.y].reference;
                            
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            Momentalni.reference = Prohledavani_btn;
                            Momentalni.hodnota = hra.hracipole[Momentalni.x - 1, Momentalni.y].hodnota;
                            Momentalni.x = Momentalni.x - 1;
                            Fronta.Enqueue(Momentalni);
                            Momentalni.x = Momentalni.x + 1;
                            odkryto++;
                        }
                        int k = hra.hracipole[Momentalni.x - 1, Momentalni.y].hodnota;
                        
                        if (k > 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x - 1, Momentalni.y].reference;
                            Prohledavani_btn.Content = hra.hracipole[Momentalni.x - 1, Momentalni.y].hodnota;
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            odkryto++;
                        }
                    }
                    hra.hracipole[Momentalni.x - 1, Momentalni.y].BylJsemTu++;
                }
                
                if ((Momentalni.x >= 0 && Momentalni.y + 1 >= 0) && (Momentalni.x < hra.hracipole.GetLength(0) && Momentalni.y + 1 < hra.hracipole.GetLength(1)) && hra.hracipole[Momentalni.x, Momentalni.y + 1].hodnota != -1)
                {
                    
                    if (hra.hracipole[Momentalni.x, Momentalni.y + 1].BylJsemTu < 1)
                    {
                        if (hra.hracipole[Momentalni.x, Momentalni.y + 1].hodnota == 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x, Momentalni.y + 1].reference;
                            
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            Momentalni.reference = Prohledavani_btn;
                            Momentalni.hodnota = hra.hracipole[Momentalni.x, Momentalni.y + 1].hodnota;
                            Momentalni.y = Momentalni.y + 1;
                            Fronta.Enqueue(Momentalni);
                            Momentalni.y = Momentalni.y - 1;
                            odkryto++;
                        }
                        int k = hra.hracipole[Momentalni.x, Momentalni.y + 1].hodnota;
                        
                        if (k > 0)
                        {
                            Prohledavani_btn = hra.hracipole[Momentalni.x, Momentalni.y + 1].reference;
                            Prohledavani_btn.Content = hra.hracipole[Momentalni.x, Momentalni.y + 1].hodnota;
                            Prohledavani_btn.Background = Brushes.LightGreen;
                            odkryto++;
                        }
                    }
                    hra.hracipole[Momentalni.x, Momentalni.y + 1].BylJsemTu++;
                }
            }
            
            
        }

        private void Odeber()
        {
            MainDock.Children.Remove(VelikostHry);
            MainDock.Children.Remove(InfoVyber);
        }

        private void PridejGrid(Pole hra, int Velikost)
        {
            var GridHra = new Grid();
            GridHra.Name = "HerniPole_Grid";
            GridHra.VerticalAlignment = VerticalAlignment.Top;
            GridHra.HorizontalAlignment = HorizontalAlignment.Center;
            GridHra.ShowGridLines = false;
            GridHra.Background = new SolidColorBrush(Colors.Blue);
            GridHra.Margin = new Thickness(0,0,0,60);
            for (int Col = 0; Col < hra.GetX; Col++)
            {
                ColumnDefinition GridColl = new ColumnDefinition();
                GridColl.Width = new GridLength(Velikost);
                GridHra.ColumnDefinitions.Add(GridColl);
            }
            for (int Row = 0; Row < hra.GetY; Row++)
            {
                RowDefinition GridRow = new RowDefinition();
                GridRow.Height = new GridLength(Velikost);
                GridHra.RowDefinitions.Add(GridRow);
            }
            DockPanel.SetDock(GridHra, Dock.Bottom);
            MainDock.Children.Add(GridHra);
            this.GridHra = GridHra;
            //pridavani bomb
            for (int y = 0; y < hra.GetY; y++)
            {
                for (int x = 0; x < hra.GetX; x++)
                {
                    Tlacitka a;
                    a.x = x;
                    a.y = y;
                    a.hodnota = 0;
                    var btn_hra = new Button();
                    a.reference = btn_hra;
                    hra.hracipole[a.x,a.y].reference = btn_hra;
                    a.BylJsemTu = 0;
                    btn_hra.Tag = a;
                    btn_hra.Click += btn_hra_Click;
                    if(x % 2 == 0)
                    {
                        if (y % 2 == 0)
                        {
                            btn_hra.Background = new SolidColorBrush(Colors.DeepSkyBlue);
                        }
                        else
                        {
                            btn_hra.Background = new SolidColorBrush(Colors.LightSeaGreen);
                        }
                    }
                    if (x % 2 == 1)
                    {
                        if (y % 2 == 1)
                        {
                            btn_hra.Background = new SolidColorBrush(Colors.DeepSkyBlue);
                        }
                        else
                        {
                            btn_hra.Background = new SolidColorBrush(Colors.LightSeaGreen);
                        }
                    }
                    btn_hra.MouseRightButtonDown += Right_click_btn_hra;
                    Grid.SetRow(btn_hra, y);
                    Grid.SetColumn(btn_hra, x);
                    GridHra.Children.Add(btn_hra);
                }
            }

        }

        private void UI(int bomb)
        {
            // stackpanel s UI
            var StackP = new StackPanel();
            StackP.Name = "Option";
            StackP.Orientation = Orientation.Horizontal;
            StackP.Height = 50;
            DockPanel.SetDock(StackP, Dock.Top);
            MainDock.Children.Add(StackP);
            this.StackP = StackP;
            // ukoncuje hru
            var END = new Button();
            END.Name = "END_btn";
            END.Click += Click_END_btn;
            END.Margin = new Thickness(10, 0, 0, 10);
            END.Content = "Quit";
            END.Padding = new Thickness(5, 5, 5, 5);
            StackP.Children.Add(END);
            // vytvori novou hru
            var NEW = new Button();
            NEW.Name = "NEW_btn";
            this.NEW = NEW;
            NEW.Click += Click_NEW_btn;
            NEW.Margin = new Thickness(10, 0, 0, 10);
            NEW.Content = "New Game";
            NEW.Padding = new Thickness(5, 5, 5, 5);
            StackP.Children.Add(NEW);
            // pocet bomb
            var BombCounter = new TextBlock();
            BombCounter.HorizontalAlignment = HorizontalAlignment.Center;
            BombCounter.VerticalAlignment = VerticalAlignment.Center;
            BombCounter.TextAlignment = TextAlignment.Center;
            BombCounter.FontSize = 20;
            BombCounter.Margin = new Thickness(30, 0, 0, 10);
            BombCounter.Text = "⚑  " + bomb.ToString();
            this.bombcounter = BombCounter;
            StackP.Children.Add(BombCounter);
        }   


        private void Velke_click(object sender, RoutedEventArgs e)
        {
            konec = 0;
            Last = 3;
            int bomb = 75;
            this.bombO= bomb;
            this.bomb = bomb;
            int Velikost = 25;
            Pole hra = new Pole(20, 18, bomb);
            this.hra = hra;
            Odeber();
            PridejGrid(hra,Velikost);
            UI(bomb);
        }

        private void Stredni_click(object sender, RoutedEventArgs e)
        {
            konec = 0;
            Last = 2;
            int bomb = 50;
            this.bombO = bomb;
            this.bombO = bomb;
            this.bomb = bomb;
            int Velikost = 30;
            Pole hra = new Pole(18, 14, bomb);
            this.hra = hra;
            Odeber();
            PridejGrid(hra, Velikost);
            UI(bomb);
        }

        private void Male_click(object sender, RoutedEventArgs e)
        {
            konec = 0;
            Last = 1;
            int bomb = 12;
            this.bombO = bomb;
            this.bomb = bomb;
            int Velikost = 45;
            Pole hra = new Pole(10, 8, bomb);
            this.hra = hra;
            Odeber();
            PridejGrid(hra, Velikost);
            UI(bomb);
        }

        private void Click_END_btn(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }
        private void Click_NEW_btn(object sender, RoutedEventArgs e)
        {
            MainDock.Children.Remove(GridHra);
            MainDock.Children.Remove(StackP);
            this.Round = 0;
            konec = 0;
            if (NewGame != 1)
            {

                if (Last == 3)
                {
                    Velke_click(sender, e);
                }
                if (Last == 2)
                {
                    Stredni_click(sender, e);
                }
                if (Last == 1)
                {
                    Male_click(sender, e);
                }
            }
            else
            {
                //MainDock.Children.Remove(EndInfo);
                if (Last == 3)
                {
                    Velke_click(sender, e);
                }
                if (Last == 2)
                {
                    Stredni_click(sender, e);
                }
                if (Last == 1)
                {
                    Male_click(sender, e);
                }
            }
        }

        private void Right_click_btn_hra(object sender, RoutedEventArgs e)
        {
            Button nevim = (Button)sender;
            if (nevim.Content == "⚑" || nevim.Content == null)
            {
                if (nevim.Content == "⚑")
                {
                    nevim.Content = null;
                    bomb = bomb + 1;
                    bombcounter.Text = "⚑" + bomb.ToString();
                }
                else
                {
                    nevim.Content = "⚑";
                    bomb = bomb - 1;
                    bombcounter.Text = "⚑" + bomb.ToString();
                }
            }
            
            
        }

        private void btn_hra_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            Tlacitka c = (Tlacitka)btn.Tag;
            
            if (Round == 0 && konec != 1)
            {
                hra.Generovani(c.x, c.y);
                OdhalPole(c);
                OdhaleniHran(c);
                Round = 1;
                konec = 0;
                

            }
            else
            {
                PoPrvnimTahu = 1;
            }
            if (hra.hracipole[c.x, c.y].hodnota != -1 && konec != 1)
            {
                if (btn.Content != "⚑")
                {
                    if (hra.hracipole[c.x, c.y].hodnota == 0 )
                    {
                         
                    
                        if (hra.hracipole[c.x, c.y].hodnota == 0 && PoPrvnimTahu == 1)
                        {
                            Fronta.Enqueue(c);
                            odkryto++;
                            btn.Background = Brushes.LightGreen; 
                            OdhaleniHran(c);
                        }
                    }
                    else {
                        btn.Content = hra.hracipole[c.x, c.y].hodnota;
                        hra.hracipole[c.x, c.y].BylJsemTu++;
                        btn.Background = Brushes.LightGreen;
                        odkryto++;

                    }
                }
                
            }
            else
            {
                if (btn.Content != "⚑")
                {
                    konec = 1;
                    for (int y = 0; y < hra.hracipole.GetLength(1); y++)
                    {
                        for (int x = 0; x < hra.hracipole.GetLength(0); x++)
                        {
                            if (hra.hracipole[x,y].hodnota == -1 )
                            {
                                Button pomoc = new Button();
                                pomoc = hra.hracipole[x, y].reference;
                                pomoc.Content = "💣";
                                pomoc.Background = Brushes.OrangeRed;
                            }
                        }
                    }
                    
                }
                
            }
            if (odkryto == (hra.GetX * hra.GetY) - bombO)
            {
                MainDock.Children.Remove(StackP);
                MainDock.Children.Remove(GridHra);
                var info = new TextBlock();
                info.HorizontalAlignment = HorizontalAlignment.Center;
                info.VerticalAlignment = VerticalAlignment.Center;
                info.TextAlignment = TextAlignment.Center;
                info.FontSize = 20;
                info.Margin = new Thickness(30, 0, 0, 10);
                info.Text = "You won!!";
                MainDock.Children.Add(info);

            }
        }
    }


    public class Pole
    {
        private int x;
        private int y;
        private Random rnd;
        public MainWindow.Tlacitka[,] hracipole;
        private int bomb;

        public int GetX
        {
            get
            {
                return this.x;
            } 
        }
        public int GetY
        {
            get
            {
                return this.y;
            }
        }
        
        public Pole(int x, int y, int bomb)
        {
            this.x = x;
            this.y = y;
            this.rnd = new Random();
            this.hracipole = new MainWindow.Tlacitka[x, y];
            this.bomb = bomb;
        }

        public void Generovani(int x, int y)
        {
            NastaveniBomb(bomb, x, y);
            CislickaSousedi(x, y);
        }

        private void NastaveniBomb(int bomb, int Xnemuze, int Ynemuze)
        {
            int rndX;
            int rndY;
            
            for (int i = 0; i < bomb; i++)
            {
                rndX = rnd.Next(this.x);
                rndY = rnd.Next(this.y);
                if (hracipole[rndX,rndY].hodnota != (-1) && (rndX != Xnemuze && rndY != Ynemuze) && (rndX != Xnemuze - 1 && rndY != Ynemuze - 1) && (rndX != Xnemuze  && rndY != Ynemuze - 1) && (rndX != Xnemuze + 1 && rndY != Ynemuze - 1) && (rndX != Xnemuze - 1 && rndY != Ynemuze) && (rndX != Xnemuze + 1 && rndY != Ynemuze) && (rndX != Xnemuze - 1 && rndY != Ynemuze + 1) && (rndX != Xnemuze && rndY != Ynemuze + 1) && (rndX != Xnemuze + 1 && rndY != Ynemuze + 1) )
                {
                    hracipole[rndX, rndY].hodnota = -1;
                    hracipole[rndX, rndY].x = rndX;
                    hracipole[rndX, rndY].y = rndY;
                }
                else
                {
                    i = i - 1;
                }
            }
        }
        
        private void CislickaSousedi(int Xnemuze, int Ynemuze)
        {
            for (int x = 0; x < this.x; x++)
            {
                for (int y = 0; y < this.y; y++)
                {
                    if (hracipole[x,y].hodnota != -1)
                    {
                        int counter = 0;
                        if ((x - 1 >= 0 && y - 1 >= 0) && (x - 1 < this.x && y - 1 < this.y) && hracipole[x - 1, y - 1].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x >= 0 && y - 1 >= 0) && (x < this.x && y - 1 < this.y) && hracipole[x, y - 1].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x + 1 >= 0 && y - 1 >= 0) && (x + 1 < this.x && y - 1 < this.y) && hracipole[x + 1, y - 1].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x - 1 >= 0 && y >= 0) && (x - 1 < this.x && y < this.y) && hracipole[x - 1, y].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x + 1 >= 0 && y >= 0) && (x + 1 < this.x && y < this.y) && hracipole[x + 1, y].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x - 1 >= 0 && y + 1 >= 0) && (x - 1 < this.x && y + 1 < this.y) && hracipole[x - 1, y + 1].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x >= 0 && y + 1 >= 0) && (x < this.x && y + 1 < this.y) && hracipole[x , y + 1].hodnota == -1)
                        {
                            counter++;
                        }
                        if ((x + 1 >= 0 && y + 1 >= 0) && (x + 1 < this.x && y + 1 < this.y) && hracipole[x + 1, y + 1].hodnota == -1)
                        {
                            counter++;
                        }
                        hracipole[x, y].hodnota = counter;
                        hracipole[x, y].x = x;
                        hracipole[x, y].y = y;
                    }
                }
            }
         }
      }
}
